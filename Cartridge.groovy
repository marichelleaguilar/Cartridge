folder( "AverageWebApp" ){
}

def folder = "AverageWebApp"



def generatejob = folder + "/Build_Job"
freeStyleJob(generatejob){

    steps{
		scm{
			git{
				branch('*/master')
				remote{
					url('https://marichelleaguilar@gitlab.com/marichelleaguilar/AverageWebApp.git')
					credentials('e4a3a0be-5594-4335-a4a5-09d300ada0b3')
				}
			}
		}
		triggers{
			 gitlabPush {
				buildOnMergeRequestEvents(true)
				buildOnPushEvents(true)
				enableCiSkip(true)
				setBuildDescription(true)
				rebuildOpenMergeRequest('never')
			}
		}

		maven{
			mavenInstallation('ADOP Maven')
			goals('package')
		}
		
		wrappers {
			preBuildCleanup {
				includePattern('**/target/**')
				deleteDirectories()
				cleanupParameter('CLEANUP')
			}
		}
		
		publishers{
			extendedEmail {
				recipientList('4r1c4r1c@gmail.com')
				replyToList('$DEFAULT_REPLYTO')
				defaultContent('$DEFAULT_CONTENT')
					triggers{
						failure{
							attachBuildLog(true)
							content('$PROJECT_DEFAULT_CONTENT')
							sendTo{
								recipientList()
							}
						}	
					}
			}
			downstreamParameterized {
				trigger('Code_Analysis') {
					condition('SUCCESS')
					parameters {
						predefinedProp('CUSTOM_WORKSPACE', '$WORKSPACE')
						predefinedProp('CUSTOM_BUILD_ID', '$BUILD_ID')
					}
				}
			}
		}
	}
}

def generatejob2 = folder + "/Code_Analysis"
freeStyleJob(generatejob2){

	steps{
		parameters {
			stringParam('CUSTOM_WORKSPACE')
			stringParam('CUSTOM_BUILD_ID')
		}
		configure {
			it / 'builders' << 'hudson.plugins.sonar.SonarRunnerBuilder' {
			properties 'sonar.projectKey=AverageWebApp\nsonar.projectVersion=$CUSTOM_BUILD_ID\nsonar.projectName=Web App Average\nsonar.web.url=http://localhost:9000\nsonar.sources=.'
			javaOpts ''
			jdk '(Inherit From Job)'
			project ''
			task ''
			}
		}
		publishers{
			extendedEmail {
				recipientList('4r1c4r1c@gmail.com')
				replyToList('$DEFAULT_REPLYTO')
				defaultContent('$DEFAULT_CONTENT')
					triggers{
						failure{
							attachBuildLog(true)
							content('$PROJECT_DEFAULT_CONTENT')
							sendTo{
								recipientList()
							}
						}	
					}
			}
		
			downstreamParameterized {
				trigger('Deploy') {
					condition('SUCCESS')
					parameters {
						predefinedProp('CUSTOM_WORKSPACE', '$WORKSPACE')
						predefinedProp('CUSTOM_BUILD_ID', '$BUILD_ID')
					}
				}
			}
		}
	}
}

def generatejob3 = folder + "/Deploy"
freeStyleJob(generatejob3){
	steps
	{
		parameters {
			stringParam('CUSTOM_WORKSPACE')
			stringParam('CUSTOM_BUILD_ID')
		}
		scm
		{
			git
			{
				branch('master')
				remote{
					url('git@gitlab:4R1C/ANSIBLE_PLAYBOOK.git')
					credentials('adop-jenkins-master')
				}
			}
		}
		batchFile('copy C:\Users\ma.r.b.aguilar\.jenkins\workspace\AverageWebApp\Build_Job\target\webapp.war "C:\Users\ma.r.b.aguilar\Downloads\apache-tomcat-7.0.78\webapps"')
	}
}
/*
def generatejob3 = folder + "/Deploy"
freeStyleJob(generatejob3){
	steps
	{
		parameters {
			stringParam('CUSTOM_WORKSPACE')
			stringParam('CUSTOM_BUILD_ID')
		}
		label('ansible')
		scm
		{
			git
			{
				branch('master')
				remote{
					url('git@gitlab:4R1C/ANSIBLE_PLAYBOOK.git')
					credentials('adop-jenkins-master')
				}
			}
		}
		shell ('ansible-playbook -i hosts recreate.yml -u ec2-user')
		publishers{
			extendedEmail {
				recipientList('4r1c4r1c@gmail.com')
				replyToList('$DEFAULT_REPLYTO')
				defaultContent('$DEFAULT_CONTENT')
					triggers{
						failure{
							attachBuildLog(true)
							content('$PROJECT_DEFAULT_CONTENT')
							sendTo{
								recipientList()
							}
						}	
					}
			}
			downstreamParameterized {
				trigger('SeleniumTest') {
					condition('SUCCESS')
					parameters {
						predefinedProp('CUSTOM_WORKSPACE', '$WORKSPACE')
						predefinedProp('CUSTOM_BUILD_ID', '$BUILD_ID')
					}
				}
			}
		}
	}
}

def generatejob4 = folder + "/SeleniumTest"
freeStyleJob(generatejob4)
{
	steps
	{
		parameters {
			stringParam('CUSTOM_WORKSPACE')
			stringParam('CUSTOM_BUILD_ID')
		}
		scm
		{
			git
			{
				branch('master')
				remote
				{
					url('git@gitlab:4R1C/SeleniumTest.git')
					credentials('bf5ad93f-1751-4724-853d-0dc3c0ab6486')
				}
			}
		}
		
		shell("java -jar SeleniumTestver1.jar")
		
		publishers{
			extendedEmail {
				recipientList('4r1c4r1c@gmail.com')
				replyToList('$DEFAULT_REPLYTO')
				defaultContent('$DEFAULT_CONTENT')
					triggers{
						failure{
							attachBuildLog(true)
							content('$PROJECT_DEFAULT_CONTENT')
							sendTo{
								recipientList()
							}
						}	
					}
			}
			downstreamParameterized {
				trigger('Upload_Artifact') {
					condition('SUCCESS')
					parameters {
						predefinedProp('CUSTOM_WORKSPACE', '$WORKSPACE')
						predefinedProp('CUSTOM_BUILD_ID', '$BUILD_ID')
					}
				}
			}
		}
	}
}



def generatejob5= folder + "/Upload_Artifact"
freeStyleJob(generatejob5)
{
	steps
	{
		parameters {
			stringParam('CUSTOM_WORKSPACE')
			stringParam('CUSTOM_BUILD_ID')
		}
		scm
		{
			git
			{
				branch('master')
				remote
				{
					url('git@gitlab:4R1C/AverageWebApp.git')
					credentials('adop-jenkins-master')
				}
			}
		}
		nexusArtifactUploader
		{
        	nexusVersion('NEXUS')
        	protocol('HTTP')
        	nexusUrl('13.56.92.148/nexus/content/repositories/releases/')
        	groupId('4R1C')
        	version('$BUILD_ID')
        	credentialsId('bf5ad93f-1751-4724-853d-0dc3c0ab6486')
        	repository('Releases')

        	artifact
        	{
            	artifactId('webapp')
            	type('war')
            	classifier('')
            	file('/var/jenkins_home/jobs/AverageWebApp/jobs/Build_Job/workspace/target/webapp.war')
        	}
        }
		publishers{
			extendedEmail {
				recipientList('4r1c4r1c@gmail.com')
				replyToList('$DEFAULT_REPLYTO')
				defaultContent('$DEFAULT_CONTENT')
					triggers{
						failure{
							attachBuildLog(true)
							content('$PROJECT_DEFAULT_CONTENT')
							sendTo{
								recipientList()
							}
						}	
					}
			}
		}
	}
} 
*/
buildPipelineView('AverageWebApp/AVERAGE_PIPELINE') {
    filterBuildQueue()
    filterExecutors()
    displayedBuilds(1)
    selectedJob('AverageWebApp/Build_Job')
    refreshFrequency(5)
}